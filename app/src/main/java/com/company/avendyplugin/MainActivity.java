package com.company.avendyplugin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.company.avendyplugin.utils.Avendy;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AvendyButton avendyButton = findViewById(R.id.avendy_button);
        avendyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Avendy.loginAndShare(MainActivity.this, "productTitle", "productDescription", "productImage");
            }
        });
    }
}
