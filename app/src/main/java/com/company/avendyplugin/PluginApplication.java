package com.company.avendyplugin;

import android.app.Application;

/**
 * Created by Sitaram on 6/12/2018.
 */

public class PluginApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AvendyPluginSetting.enableSharing(this);
    }
}
