package com.company.avendyplugin;

import android.content.Context;
import android.util.AttributeSet;

import com.company.avendyplugin.R;

public class AvendyButton extends android.support.v7.widget.AppCompatImageView {

    public AvendyButton(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public AvendyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        this.setImageResource(R.drawable.ic_avendy_button);
    }

    public AvendyButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }
}
