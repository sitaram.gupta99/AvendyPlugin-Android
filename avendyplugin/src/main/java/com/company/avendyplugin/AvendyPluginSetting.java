package com.company.avendyplugin;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Sitaram on 6/19/2018.
 */

public class AvendyPluginSetting {

    private static AvendyPluginSetting appSettings;
    private static Context context;

    private static final String SP_NAME = "AvendyPlugin";
    private static final String SP_USER_ID = "userId";

    private SharedPreferences sharedPreference;

    public static void enableSharing(Context mContext) {
        context = mContext;
    }


    public static AvendyPluginSetting getInstance() {
        if (appSettings == null) {
            synchronized (AvendyPluginSetting.class) {
                appSettings = new AvendyPluginSetting();
                if (context == null) {
                    Log.e("Avendy Error: ", "AvendyPluginSetting.enableSharing(this); method is missing in Application class");
                }
            }
        }
        return appSettings;
    }

    public SharedPreferences getSharedPreference() {
        if (sharedPreference == null) {
            synchronized (AvendyPluginSetting.class) {
                sharedPreference = context.getSharedPreferences(SP_NAME, MODE_PRIVATE);
            }
        }
        return sharedPreference;
    }

    public SharedPreferences.Editor getSharedPreferenceEditor() {
        return getSharedPreference().edit();
    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void setUserIdPref(String token) {
        getSharedPreferenceEditor().putString(SP_USER_ID, token).commit();
    }

    public String getUserIdFromPref() {
        return getSharedPreference().getString(SP_USER_ID, "");
    }
}
