package com.company.avendyplugin.interfaces;

/**
 * Created by Shilu Shrestha on 3/29/17.
 */

public interface ViewUpdateListener {
    void success();

    void failure(String msg);
}
