package com.company.avendyplugin.network;

import android.content.Context;

import com.company.avendyplugin.model.ProductDetails;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Sitaram on 18/6/2018.
 * <p>
 * Base class for retrofit.
 */
public class RetrofitManager {

    private static RetrofitManager retrofitManager;

    private final RetrofitService service;
    private Retrofit retrofit;

    public static RetrofitManager getInstance(Context mContext, boolean debugMode) {
        String baseUrl;
        if (debugMode) {
            baseUrl = "https://chitooo-dev-api.herokuapp.com/";
        } else {
            baseUrl = "https://chitooo-release.appspot.com/";
        }
        // for changing url we need to check this
        if (retrofitManager == null)
            retrofitManager = new RetrofitManager(baseUrl);

        return retrofitManager;
    }

    RetrofitManager(String baseUrl) {
        // for logging
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(180, TimeUnit.SECONDS).build();
        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(RetrofitService.class);
    }

    public void branchApiCall(ProductDetails productDetails, Callback<JsonObject> onFailure) {
        Call<JsonObject> branchApiCall = service.branchApiCall(productDetails);
        branchApiCall.enqueue(onFailure);
    }
}