package com.company.avendyplugin.network;

import com.company.avendyplugin.model.ProductDetails;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Sitaram on 18/6/2018.
 */
public interface RetrofitService {
    /*
     * RetrofitService get annotation with our URL
     *
     */

    @POST("branch/create")
    Call<JsonObject> branchApiCall(@Body ProductDetails productDetails);
}
