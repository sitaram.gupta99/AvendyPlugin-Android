package com.company.avendyplugin.utils;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import com.company.avendyplugin.model.ProductDetails;
import com.company.avendyplugin.view.WebLoginPopupView;

/**
 * Created by Sitaram on 6/11/2018.
 */

public class Avendy {

    public static void loginAndShare(Activity activity, String productTitle, String productDescription, String productImage) {

        ApplicationInfo ai = null;
        try {
            ai = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Bundle bundle = ai.metaData;
        String clientId = bundle.getString("com.company.avendyplugin.ClientId");
        Log.d("aaaa", "clientId: " + clientId);

        ProductDetails productDetails = new ProductDetails();
        productDetails.setProductTitle(productTitle);
        productDetails.setProductDescription(productDescription);
        productDetails.setProductImage(productImage);
        productDetails.setClientId(clientId);

        WebLoginPopupView webLoginPopupView = new WebLoginPopupView(activity, productDetails);
        webLoginPopupView.show();
    }
}
