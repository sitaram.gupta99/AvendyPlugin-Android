package com.company.avendyplugin.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Sitaram on 18/6/2018.
 * <p>
 */
public class DialogUtils {
    private static ProgressDialog progressDialog;

    /**
     * Returns simple progress dialog with message.
     *
     * @param context {@link Context} of the initiating activity
     * @param title   {@link String} title of the progress dialog
     * @param msg     {@link String} message to show in progress
     * @return {@link ProgressDialog}
     */
    public static ProgressDialog showProgressDialog(final Context context, String title, String msg, boolean indeterminate, boolean cancelable) {
        progressDialog = ProgressDialog.show(context,
                title,
                msg, indeterminate, cancelable,
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Activity activity = (Activity) context;
                        activity.finish();
                    }
                }
        );

        return progressDialog;
    }
}
