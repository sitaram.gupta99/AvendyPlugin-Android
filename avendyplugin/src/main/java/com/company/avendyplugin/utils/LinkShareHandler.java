package com.company.avendyplugin.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.util.Log;

import com.company.avendyplugin.AvendyPluginSetting;
import com.company.avendyplugin.R;
import com.company.avendyplugin.model.ProductDetails;
import com.company.avendyplugin.network.RetrofitManager;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sitaram on 6/11/2018.
 */

public class LinkShareHandler {

    private static final String KEY_URL = "url";
    private static ProgressDialog progressDialog;


    public static void createAndShare(final Context mContext, ProductDetails productDetails) {
        ApplicationInfo ai = null;
        try {
            ai = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Bundle bundle = ai.metaData;
        boolean debugMode = bundle.getBoolean("com.company.avendyplugin.DebugMode");

        progressDialog = DialogUtils.showProgressDialog(mContext, null, mContext.getResources().getString(R.string.msg_progress_loading), true, true);
        RetrofitManager.getInstance(mContext, debugMode).branchApiCall(productDetails, new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().has(KEY_URL)) {
                        Log.d("aaaa", "link: " + response.body().get(KEY_URL).getAsString());
                        ShareCompat.IntentBuilder.from((Activity) mContext)
                                .setType("text/plain")
                                .setChooserTitle("Refer with Avendy")
                                .setText(response.body().get(KEY_URL).getAsString())
                                .startChooser();
                    } else {
                        AvendyPluginSetting.getInstance().showToast(mContext.getResources().getString(R.string.msg_error));
                    }
                } else {
                    AvendyPluginSetting.getInstance().showToast(mContext.getResources().getString(R.string.msg_error));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (progressDialog != null)
                    progressDialog.dismiss();
                Log.e("onFailure", t.toString());
            }
        });
    }
}
