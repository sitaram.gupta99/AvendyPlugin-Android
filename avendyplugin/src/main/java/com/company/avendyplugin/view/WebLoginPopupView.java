package com.company.avendyplugin.view;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.company.avendyplugin.AvendyPluginSetting;
import com.company.avendyplugin.R;
import com.company.avendyplugin.model.ProductDetails;
import com.company.avendyplugin.utils.LinkShareHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sitaram on 2/28/2018.
 */

public class WebLoginPopupView extends Dialog {
    public static final String TAG = WebLoginPopupView.class.getSimpleName();

    private final View view;
    private final WebView myWebView;
    private final ProgressBar progressbar;

    public WebLoginPopupView(final Activity activity, final ProductDetails productDetails) {
        super(activity);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        view = LayoutInflater.from(activity).inflate(R.layout.web_login_popup, null);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setContentView(view, params);
        setCanceledOnTouchOutside(false);

        progressbar = findViewById(R.id.pb_progressbar);

        myWebView = findViewById(R.id.web_view);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        webSettings.setAllowFileAccess(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        myWebView.setWebViewClient(new MyWebViewClient());
        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("aaaaa MyApplication", consoleMessage.message() + " -- From line "
                        + consoleMessage.lineNumber() + " of "
                        + consoleMessage.sourceId());
                if (consoleMessage.message().contains("uid")) {
                    try {
                        Log.d(TAG, "aaaa Uid: " + new JSONObject(consoleMessage.message()).get("uid"));
                        productDetails.setUserId(new JSONObject(consoleMessage.message()).get("uid").toString());
                        Log.d(TAG, "aaaa Name: " + new JSONObject(consoleMessage.message()).get("name"));
                        productDetails.setUserName(new JSONObject(consoleMessage.message()).get("name").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    dismiss();
                    LinkShareHandler.createAndShare(activity, productDetails);
                }
                return super.onConsoleMessage(consoleMessage);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                WebView newWebView = new WebView(activity);
                newWebView.getSettings().setJavaScriptEnabled(true);
                newWebView.getSettings().setSupportZoom(true);
                newWebView.getSettings().setBuiltInZoomControls(true);
                newWebView.getSettings().setDomStorageEnabled(true);
                newWebView.getSettings().setAllowFileAccess(true);
                newWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                newWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
                newWebView.getSettings().setSupportMultipleWindows(true);
                view.addView(newWebView);
                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(newWebView);
                resultMsg.sendToTarget();

                newWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        view.loadUrl(url);
                        return true;
                    }
                });

                return true;
            }
        });
        myWebView.loadUrl("http://app.avendy.co/");
    }

    class MyWebViewClient extends WebViewClient {

//        @Override
//        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            view.loadUrl(url);
//            return true;
//        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressbar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressbar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (myWebView.canGoBack()) {
            myWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
